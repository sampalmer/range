﻿using SamPalmer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ranges
{
	/// <summary>
	/// Todo
	///	-Union
	///	-Reversal (should this also swap the inclusiveness of each side, making the enumerated values different?)
	///	-Unboundedness
	/// </summary>
	/// <remarks>
	/// References:
	/// http://www.codeproject.com/Articles/19028/Building-a-Generic-Range-class
	/// http://csharpindepth.com/Articles/Chapter6/Ranges.aspx
	/// http://ryanfarley.com/blog/archive/2004/08/19/966.aspx
	/// http://blogs.msdn.com/b/jaybaz_ms/archive/2004/08/19/217226.aspx
	/// http://msmvps.com/blogs/jon_skeet/archive/2008/01/26/immutability-and-inheritance.aspx
	/// </remarks>
	public class Range<T>
		where T : IComparable<T>
	{
		private readonly Bound<T> start;
		private readonly Bound<T> end;

		public Bound<T> Start
		{
			get
			{
				return start;
			}
		}

		public Bound<T> End
		{
			get
			{
				return end;
			}
		}

		public static bool operator ==(Range<T> a, Range<T> b)
		{
			if (Object.ReferenceEquals(a, null))
				return Object.ReferenceEquals(a, b);

			return a.Equals(b); //Also delegates null checking to Equals
		}

		public static bool operator !=(Range<T> a, Range<T> b)
		{
			return !(a == b);
		}

		public Range(T start, Inclusiveness startInclusiveness, T end, Inclusiveness endInclusiveness)
			: this(
				start,
				OffsetInclusivenessMapper.Map(Side.Start, startInclusiveness),
				end,
				OffsetInclusivenessMapper.Map(Side.End, endInclusiveness)
			)
		{
		}

		private Range(T start, Offset startOffset, T end, Offset endOffset)
			: this(new Bound<T>(start, startOffset), new Bound<T>(end, endOffset))
		{
		}

		public Range(Bound<T> start, Bound<T> end)
		{
			ReflectionUtilities.AssertNotNull(() => start, () => end);

			var isBackwards = start.CompareTo(end) > 0;
			if (isBackwards)
				throw new ArgumentException("The start cannot be after the end.");

			this.start = start;
			this.end = end;
		}

		public Range(T value)
			: this(value, Inclusiveness.Inclusive, value, Inclusiveness.Inclusive)
		{
		}

		public bool Overlaps(Range<T> other)
		{
			return start.CompareTo(other.end) < 0 && other.start.CompareTo(end) < 0;
		}

		public override string ToString()
		{
			Func<Side, Bound<T>, bool> isInclusive = (side, bound) => OffsetInclusivenessMapper.Map(side, bound.Offset) == Inclusiveness.Inclusive;
			
			var startIsInclusive = isInclusive(Side.Start, start);
			var startBracket = startIsInclusive ? '[' : '(';
			
			var endIsInclusive = isInclusive(Side.End, end);
			var endBracket = endIsInclusive ? ']' : ')';
			
			return String.Format("{0}{1}, {2}{3}", startBracket, start.Value, end.Value, endBracket);
		}

		public Range<T> IntersectionOrNull(Range<T> other)
		{
			var starts = new[] { start, other.start };
			var ends = new[] { end, other.end };
			
			var intersectionStart = starts.Max();
			var intersectionEnd = ends.Min();
			var hasIntersection = intersectionEnd.CompareTo(intersectionStart) > 0;

			return hasIntersection ? new Range<T>(intersectionStart, intersectionEnd) : null;
		}
		
		public bool IsEmpty
		{
			get
			{
				return start.Equals(end);
			}
		}

		public bool Contains(T value)
		{
			return start.CompareTo(value) < 0 && end.CompareTo(value) > 0;
		}

		public bool Contains(Range<T> other)
		{
			return IntersectionOrNull(other) == other;
		}

		public override bool Equals(object obj)
		{
			return obj != null
				&& GetType() == obj.GetType()
				&& MakeAnonymous().Equals(((Range<T>)obj).MakeAnonymous());
		}

		public override int GetHashCode()
		{
			return MakeAnonymous().GetHashCode();
		}

		private object MakeAnonymous()
		{
			return new
			{
				start,
				end
			};
		}
	}

	public static class Range
	{
		/// <remarks>
		/// Possible adjectives:
		/// -Joinable
		/// -Buttable
		/// -Abuttable
		/// -Attachable
		/// </remarks>
		public static Range<T> MakeJoinable<T>(T start, T end)
			where T : IComparable<T>
		{
			return new Range<T>(start, Inclusiveness.Inclusive, end, Inclusiveness.Exclusive);
		}

		public static Range<T> MakeInclusive<T>(T start, T end)
			where T : IComparable<T>
		{
			return new Range<T>(start, Inclusiveness.Inclusive, end, Inclusiveness.Inclusive);
		}

		public static Range<T> MakeExclusive<T>(T start, T end)
			where T : IComparable<T>
		{
			return new Range<T>(start, Inclusiveness.Exclusive, end, Inclusiveness.Exclusive);
		}
	}
}
