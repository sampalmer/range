﻿using SamPalmer.Common.Portable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ranges
{
	/// <summary>
	/// A position between two instances of <typeparamref name="T"/>
	/// </summary>
	public class Bound<T> : IComparable<Bound<T>>, IComparable<T>, IEquatable<Bound<T>>
		where T : IComparable<T>
	{
		private static readonly CompositeComparer<Bound<T>> comparer = new CompositeComparer<Bound<T>>
		{
			bound => bound.Value,
			{ bound => bound.Offset, Comparer<Offset>.Default.Compare }
		};

		public static implicit operator T(Bound<T> bound)
		{
			return bound.Value;
		}

		public Bound(T value, Offset offset)
		{
			if (value == null)
				throw new ArgumentNullException();

			Value = value;
			Offset = offset;
		}

		public T Value { get; private set; }
		public Offset Offset { get; private set; }

		public override string ToString()
		{
			var parts = new[]
			{
				"|",
				Value.ToString()
			};

			if (Offset == Offset.After)
				Array.Reverse(parts);

			return String.Join(String.Empty, parts);
		}

		public int CompareTo(Bound<T> other)
		{
			return comparer.Compare(this, other);
		}

		public int CompareTo(T other)
		{
			var otherBound = new Bound<T>(other, OppositeOffset);
			return CompareTo(otherBound);
		}

		public override bool Equals(object obj)
		{
			return obj != null
				&& GetType() == obj.GetType()
				&& Equals((Bound<T>)obj);
		}

		public bool Equals(Bound<T> other)
		{
			return other != null
				&& MakeAnonymous().Equals(other.MakeAnonymous());
		}
	
		private object MakeAnonymous()
		{
			return new
			{
				Value,
				Offset
			};
		}

		public override int GetHashCode()
		{
			return MakeAnonymous().GetHashCode();
		}

		public Offset OppositeOffset
		{
			get
			{
				return Offset == Offset.After ? Offset.Before : Offset.After;
			}
		}
	}
}
