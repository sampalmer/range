﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Ranges
{
	/// <summary>
	/// A generic object comparerer that would only use object's reference, 
	/// ignoring any <see cref="IEquatable{T}"/> or <see cref="object.Equals(object)"/>  overrides.
	/// </summary>
	class ReferenceEqualityComparer<T> : EqualityComparer<T>
	{
		private static IEqualityComparer<T> _defaultComparer;

		public new static IEqualityComparer<T> Default
		{
			get { return _defaultComparer ?? (_defaultComparer = new ReferenceEqualityComparer<T>()); }
		}

		public override bool Equals(T x, T y)
		{
			return ReferenceEquals(x, y);
		}

		public override int GetHashCode(T obj)
		{
			return RuntimeHelpers.GetHashCode(obj);
		}
	}
}
