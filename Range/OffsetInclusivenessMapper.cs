using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ranges
{
	static class OffsetInclusivenessMapper
	{
		public static Offset Map(Side side, Inclusiveness inclusiveness)
		{
			var isBefore = Map(side, inclusiveness, Inclusiveness.Inclusive);
			return isBefore ? Offset.Before : Offset.After;
		}

		public static Inclusiveness Map(Side side, Offset offset)
		{
			var isInclusive = Map(side, offset, Offset.Before);
			return isInclusive ? Inclusiveness.Inclusive : Inclusiveness.Exclusive;
		}

		private static bool Map<T>(Side side, T value, T trueValueForStart)
		{
			var isTrueIfStart = value.Equals(trueValueForStart);
			var isTrue = side == Side.Start ? isTrueIfStart : !isTrueIfStart;
			return isTrue;
		}
	}
}
