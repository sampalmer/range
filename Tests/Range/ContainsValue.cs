﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ranges;
using FluentAssertions;

namespace Tests
{
	[TestClass]
	public class ContainsValue : RangeGenerator
	{
		[TestMethod]
		public void Inside()
		{
			Test(AnyJoinable.Start.Value + 1, true);
		}

		[TestMethod]
		public void Outside()
		{
			Test(AnyJoinable.Start.Value - 1, false);
		}

		[TestMethod]
		public void AtStart()
		{
			Test(AnyJoinable.Start.Value, true);
		}

		[TestMethod]
		public void AtEnd()
		{
			Test(AnyJoinable.End.Value, false);
		}

		private void Test(int value, bool expected)
		{
			AnyJoinable.Contains(value).Should().Be(expected);
		}
	}
}
