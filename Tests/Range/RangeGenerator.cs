﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ranges;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
	[TestClass]
	public abstract class RangeGenerator
	{
		[TestInitialize]
		public void MakeRange()
		{
			AnyJoinable = Range.MakeJoinable(2, 10);
		}

		protected Range<int> AnyJoinable { get; private set; }

		protected Range<int> Equivalent
		{
			get
			{
				return new Range<int>(AnyJoinable.Start, AnyJoinable.End);
			}
		}

		protected Range<int> Different
		{
			get
			{
				return Range.MakeJoinable(AnyJoinable.Start + 1, AnyJoinable.End - 4);
			}
		}

		protected Range<int> Subrange
		{
			get
			{
				return Range.MakeJoinable(AnyJoinable.Start + 1, AnyJoinable.End - 1);
			}
		}

		protected Range<int> EmptySubrange
		{
			get
			{
				var start = Subrange.Start.Value;
				return Range.MakeJoinable(start, start);
			}
		}

		protected Range<int> Empty
		{
			get
			{
				return EmptySubrange;
			}
		}

		protected Range<int> EarlierRange
		{
			get
			{
				return Range.MakeJoinable(AnyJoinable.Start - 2, AnyJoinable.Start - 1);
			}
		}

		protected Range<int> PartiallyEarlierRange
		{
			get
			{
				return Range.MakeJoinable(AnyJoinable.Start - 1, AnyJoinable.End - 1);
			}
		}

		protected Range<int> LeftAdjacent
		{
			get
			{
				return Range.MakeJoinable(AnyJoinable.Start - 4, AnyJoinable.Start);
			}
		}

		protected Range<int> Single
		{
			get 
			{
				var value = 5;
				return new Range<int>(value);
			}
		}
	}
}
