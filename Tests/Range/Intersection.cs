﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using Ranges;

namespace Tests
{
	[TestClass]
	public class Intersection : RangeGenerator
	{
		[TestMethod]
		public void Subranges()
		{
			TestIntersectionSubrange(Subrange);
		}

		[TestMethod]
		public void InclusiveSubrange()
		{
			var inclusiveSubrange = Range.MakeInclusive(Subrange.Start.Value, Subrange.End.Value);
			TestIntersectionSubrange(inclusiveSubrange);
		}

		[TestMethod]
		public void ExclusiveSubrange()
		{
			var exclusiveSubrange = Range.MakeExclusive(Subrange.Start.Value, Subrange.End.Value);
			TestIntersectionSubrange(exclusiveSubrange);
		}

		[TestMethod]
		public void LeftAdjacentRanges()
		{
			TestIntersection(LeftAdjacent, null);
		}

		[TestMethod]
		public void LeftAdjacentInclusiveRanges()
		{
			var leftAdjacent = LeftAdjacent;
			var leftAdjacentInclusive = Range.MakeInclusive(leftAdjacent.Start.Value, leftAdjacent.End.Value);
			TestIntersection(leftAdjacentInclusive, Range.MakeInclusive(LeftAdjacent.End.Value, AnyJoinable.Start.Value));
		}

		[TestMethod]
		public void EmptySubranges()
		{
			TestIntersection(EmptySubrange, null);
		}

		[TestMethod]
		public void StartingBefore()
		{
			var partiallyEarlierRange = PartiallyEarlierRange;
			TestIntersection(partiallyEarlierRange, new Range<int>(AnyJoinable.Start, partiallyEarlierRange.End));
		}

		[TestMethod]
		public void CompletelyBefore()
		{
			TestIntersection(EarlierRange, null);
		}

		private void TestIntersection(Range<int> other, Range<int> expectedIntersection)
		{
			AnyJoinable.IntersectionOrNull(other).Should().Be(expectedIntersection);
		}

		private void TestIntersectionSubrange(Range<int> subrange)
		{
			TestIntersection(subrange, subrange);
		}
	}
}
