﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ranges;
using FluentAssertions;

namespace Tests
{
	[TestClass]
	public class Overlaps : RangeGenerator
	{
		[TestMethod]
		public void Within()
		{
			TestOverlap(AnyJoinable, Subrange, true);
		}

		[TestMethod]
		public void Disjoint()
		{
			TestOverlap(AnyJoinable, EarlierRange, false);
		}

		[TestMethod]
		public void TouchingLeft()
		{
			TestOverlap(AnyJoinable, LeftAdjacent, false);
		}

		[TestMethod]
		public void TouchingRight()
		{
			TestOverlap(LeftAdjacent, AnyJoinable, false);
		}

		private static void TestOverlap(Range<int> a, Range<int> b, bool expectedResult)
		{
			a.Overlaps(b).Should().Be(expectedResult);
		}
	}
}
