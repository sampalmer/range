﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ranges;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;

namespace Tests
{
	[TestClass]
	public class StaticHelpers
	{
		private int start;
		private int end;

		[TestInitialize]
		public void MakeEndValues()
		{
			start = 5;
			end = 7;
		}

		[TestMethod]
		public void MakeJoinable()
		{
			Test(
				() => Range.MakeJoinable(start, end),
				Offset.Before,
				Offset.Before
			);
		}

		[TestMethod]
		public void MakeInclusive()
		{
			Test(
				() => Range.MakeInclusive(start, end),
				Offset.Before,
				Offset.After
			);
		}

		[TestMethod]
		public void MakeExclusive()
		{
			Test(
				() => Range.MakeExclusive(start, end),
				Offset.After,
				Offset.Before
			);
		}

		private void Test(Func<Range<int>> make, Offset expectedStartOffset, Offset expectedEndOffset)
		{
			var range = make();
			range.Start.Should().Be(new Bound<int>(start, expectedStartOffset));
			range.End.Should().Be(new Bound<int>(end, expectedEndOffset));
		}
	}
}
