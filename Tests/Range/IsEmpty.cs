﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ranges;
using FluentAssertions;

namespace Tests
{
	[TestClass]
	public class IsEmpty : RangeGenerator
	{
		[TestMethod]
		public void TrueWhenEmpty()
		{
			Test(Empty, true);
		}

		[TestMethod]
		public void NonEmpty()
		{
			Test(AnyJoinable, false);
		}

		[TestMethod]
		public void FalseWhenSingleElement()
		{
			Test(Single, false);
		}

		private static void Test(Range<int> range, bool expected)
		{
			range.IsEmpty.Should().Be(expected);
		}
	}
}
