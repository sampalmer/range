﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ranges;
using FluentAssertions;

namespace Tests
{
	[TestClass]
	public class Equals : RangeGenerator
	{
		private static readonly Func<Range<int>, Range<int>, bool> equalsMethod = (a, b) => a.Equals(b);
		private static readonly Func<Range<int>, Range<int>, bool> equalsOperator = (a, b) => a == b;
		private static readonly Func<Range<int>, Range<int>, bool> notEqualsOperator = (a, b) => a != b;

		[TestMethod]
		public void SameValues()
		{
			Test(AnyJoinable, Equivalent, equalsMethod, true);
		}

		[TestMethod]
		public void SameValuesObjectEquals()
		{
			Test((object)AnyJoinable, (object)Equivalent, Object.Equals, true);
		}

		[TestMethod]
		public void SameValuesEqualsOperator()
		{
			Test(AnyJoinable, Equivalent, equalsOperator, true);
		}

		[TestMethod]
		public void DifferentValues()
		{
			Test(AnyJoinable, Different, equalsMethod, false);
		}

		[TestMethod]
		public void DifferentValuesOperator()
		{
			Test(AnyJoinable, Different, notEqualsOperator, true);
		}

		private void Test<T>(T a, T b, Func<T, T, bool> checkEquality, bool expectedResult)
		{
			checkEquality(a, b).Should().Be(expectedResult);
		}
	}
}
