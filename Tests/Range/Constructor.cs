﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using Ranges;

namespace Tests
{
	[TestClass]
	public class Constructor
	{
		[TestMethod]
		public void NoNullBounds()
		{
			TestNull(() => new Range<int>(null, null));
		}

		[TestMethod]
		public void NoNullValues()
		{
			TestNull(() => Range.MakeExclusive<string>(null, null));
		}

		[TestMethod]
		public void NotBackwards()
		{
			ShouldThrow<ArgumentException>(() => Range.MakeExclusive(5, -5));
		}

		private static void ShouldThrow<TException>(Action construct)
			where TException : Exception
		{
			construct.ShouldThrow<TException>();
		}

		private static void TestNull(Action construct)
		{
			ShouldThrow<ArgumentNullException>(construct);
		}
	}
}
