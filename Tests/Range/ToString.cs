﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ranges;
using FluentAssertions;

namespace Tests
{
	[TestClass]
	public class ToString
	{
		[TestMethod]
		public void Joinable()
		{
			var normalRange = Range.MakeJoinable(7, 9);
			normalRange.ToString().Should().Be("[7, 9)");
		}

		[TestMethod]
		public void ExclusiveStart()
		{
			var exclusiveRange = Range.MakeExclusive(0, 8);
			var text = exclusiveRange.ToString();
			text.Should().StartWith("(");
		}

		[TestMethod]
		public void InclusiveEnd()
		{
			var inclusiveRange = Range.MakeInclusive(1, 3);
			var text = inclusiveRange.ToString();
			text.Should().EndWith("]");
		}
	}
}
