﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ranges;
using FluentAssertions;

namespace Tests
{
	[TestClass]
	public class Contains : RangeGenerator
	{
		[TestMethod]
		public void Within()
		{
			Test(Subrange, true);
		}

		[TestMethod]
		public void Disjoint()
		{
			Test(EarlierRange, false);
		}

		[TestMethod]
		public void PartiallyWithin()
		{
			Test(PartiallyEarlierRange, false);
		}

		private void Test(Range<int> other, bool expected)
		{
			AnyJoinable.Contains(other).Should().Be(expected);
		}
	}
}
