﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ranges;
using FluentAssertions;

namespace Tests.Bound
{
	[TestClass]
	public class CompareTo
	{
		[TestMethod]
		public void LessValue()
		{
			TestDifferentValues(2, CompareResult.Less, 4);
		}

		[TestMethod]
		public void GreaterValue()
		{
			TestDifferentValues(10, CompareResult.Greater, 2);
		}

		[TestMethod]
		public void Before()
		{
			TestDifferentOffsets(Offset.Before, CompareResult.Less, Offset.After);
		}

		[TestMethod]
		public void After()
		{
			TestDifferentOffsets(Offset.After, CompareResult.Greater, Offset.Before);
		}

		[TestMethod]
		public void SameProperties()
		{
			var bound = MakeBound();
			var other = MakeBound();
			Test(bound, CompareResult.Equal, other);
		}

		private Bound<int> MakeBound()
		{
			return new Bound<int>(0, Offset.Before);
		}

		private void Test<T>(Bound<T> a, CompareResult result, Bound<T> b)
			where T : IComparable<T>
		{
			MapResult(a.CompareTo(b)).Should().Be(result);
		}

		private void TestDifferentValues<T>(T a, CompareResult result, T b)
			where T : IComparable<T>
		{
			var offset = default(Offset);
			var aBound = new Bound<T>(a, offset);
			var bBound = new Bound<T>(b, offset);
			Test(aBound, result, bBound);
		}

		private void TestDifferentOffsets(Offset a, CompareResult result, Offset b)
		{
			const int value = 0;
			var start = new Bound<int>(value, a);
			var end = new Bound<int>(value, b);
			Test(start, result, end);
		}

		private CompareResult MapResult(int value)
		{
			if (value < 0)
				return CompareResult.Less;
			else if (value == 0)
				return CompareResult.Equal;
			else
				return CompareResult.Greater;
		}

		private enum CompareResult
		{
			Less,
			Equal,
			Greater
		}
	}
}
