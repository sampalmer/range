﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Ranges;

namespace Tests.EndPoint
{
	[TestClass]
	public class Coersion
	{
		[TestMethod]
		public void Works()
		{
			var numberBound = new Bound<int>(5, Offset.After);
			
			Action<int> doNothing = num => { };
			doNothing(numberBound);
		}
	}
}
